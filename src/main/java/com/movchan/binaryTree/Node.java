package com.movchan.binaryTree;

public class Node {
    public int iData;
    public double dData;
    public Node leftChild;
    public Node rightChild;

    @Override
    public String toString() {
        return "Node{" +
                "iData=" + iData +
                ", dData=" + dData +
                '}';
    }
}

